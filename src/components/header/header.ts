import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UserLoginPage } from '../../pages/user-login/user-login';

@Component({
  selector: 'header',
  templateUrl: 'header.html'
})
export class HeaderComponent {
  
  constructor(public navCtrl: NavController,) {
    console.log('Hello HeaderComponent Component');
  }

  logout(){
    this.navCtrl.setRoot(UserLoginPage);
  }

}

import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { LoadingController, ToastController, AlertController, NavController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { AwsServiceProvider } from '../../providers/aws-service/aws-service';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { CareFacilityListPage } from '../care-facility-list/care-facility-list';

declare var google: any;

@Component({
    selector: 'page-care-facility',
    templateUrl: 'care-facility.html'
})
export class CareFacilityPage {
    @ViewChild('map') mapElement: ElementRef;
    @ViewChild('searchbar', { read: ElementRef }) searchbar: ElementRef;
    addressElement: HTMLInputElement = null;
    careFacilityForm: FormGroup;
    map: any;
    loading: any;
    options: any;
    previousMarker: any;
    carePartnerData: any = {
        guid: '',
        partnerId: '',
        parentId: '',
        name: null,
        nickname: '',
        address: null,
        city: null,
        state: null,
        postalCode: null,
        partnerLevel: '',
        status: '',
        phone: '',
        lead: '',
        notes: '',
        department: '',
        reporting: '',
        lat: null,
        lng: null,
        type: null,
        place_id: null,
        location: null
    };
    name: any;
    address: any;
    phone: any;
    eMail: any;
    place_id: any;
    flag: boolean = false;
    guid: any;

    constructor(
        public zone: NgZone,
        public loadingCtrl: LoadingController,
        public geolocation: Geolocation,
        public toastCtrl: ToastController,
        public alertCtrl: AlertController,
        public navCtrl: NavController,
        public awsServiceProvider: AwsServiceProvider,
        private formBuilder: FormBuilder, ) {
        this.careFacilityForm = this.formBuilder.group({
            partnerId: ['', Validators.compose([Validators.required])],
            parentId: ['', Validators.compose([Validators.required])],
            name: ['', Validators.compose([Validators.required])],
            nickname: ['', Validators.compose([Validators.required])],
            address: ['', Validators.compose([Validators.required])],
            city: ['', Validators.compose([Validators.required])],
            state: ['', Validators.compose([Validators.required])],
            postalCode: ['', Validators.compose([Validators.required])],
            partnerLevel: ['', Validators.compose([Validators.required])],
            status: ['', Validators.compose([Validators.required])],
            phone: ['', Validators.compose([Validators.required])],
            lead: ['', Validators.compose([Validators.required])],
            notes: ['', Validators.compose([Validators.required])],
            department: ['', Validators.compose([Validators.required])],
            reporting: ['', Validators.compose([Validators.required])]
        });
    }

    ionViewDidLoad() {
        this.loadMap();
    }

    loadMap() {
        if (!!google) {
            this.initializeMap();
            this.initAutocomplete();
        } else {
            console.log('Error', 'Something went wrong with the Internet Connection. Please check your Internet.')
        }
    }

    initializeMap() {
        this.zone.run(() => {
            var mapEle = this.mapElement.nativeElement;
            this.map = new google.maps.Map(mapEle, {
                zoom: 10,
                center: { lat: 51.165691, lng: 10.451526 },
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [],
                disableDoubleClickZoom: false,
                disableDefaultUI: true,
                zoomControl: false,
                scaleControl: true,
            });
        });
    }

    initAutocomplete(): void {
        this.addressElement = this.searchbar.nativeElement.querySelector('.searchbar-input');
        this.createAutocomplete(this.addressElement).subscribe((location) => {
            this.options = {
                center: location,
                zoom: 17
            };
        });
    }

    createAutocomplete(addressEl: HTMLInputElement): Observable<any> {
        const autocomplete = new google.maps.places.Autocomplete(addressEl);
        autocomplete.bindTo('bounds', this.map);
        return new Observable((sub: any) => {
            google.maps.event.addListener(autocomplete, 'place_changed', () => {
                const place = autocomplete.getPlace();
                if (!place.geometry) {
                    sub.error({
                        message: 'Autocomplete returned place with no geometry'
                    });
                } else {
                    var address = place.formatted_address
                    let locationArr = address.split(',');
                    let chars = locationArr[2].match(/[a-zA-Z]/g);
                    let _state = "";
                    chars.forEach(element => {
                        _state = _state + element;
                    });
                    let _postal_code;
                    let pc = null;
                    for (var i = 0; i < locationArr.length; i++) {
                        pc = locationArr[i].match(/\d+/);
                        if (pc != null) {
                            _postal_code = pc[0];
                        }
                    }
                    console.log(_postal_code);
                    this.carePartnerData = {
                        lat: place.geometry.location.lat(),
                        lng: place.geometry.location.lng(),
                        address: place.formatted_address,
                        type: place.types[0],
                        name: place.name,
                        guid: place.id,
                        place_id: place.place_id,
                        phone: place.formatted_phone_number,
                        state: _state,
                        city: locationArr[1],
                        postalCode: _postal_code,
                        location: locationArr[0]
                    }
                    console.log(this.carePartnerData);
                    // `
                    // Name:${place.name},Address:${place.formatted_address},PhoneNumber:${place.formatted_phone_number}
                    // `   
                    sub.next(place.geometry.location);
                }
            });
        });
    }

    view() {
        this.navCtrl.push(CareFacilityListPage);
    }

    go() {
        if (this.carePartnerData.name != null && this.carePartnerData.guid != null) {
            this.name = this.carePartnerData.name;
            this.address = this.carePartnerData.address;
            this.phone = this.carePartnerData.phone;
            // this.eMail = 'NA';
            // this.place_id = this.carePartnerData.place_id;
            let url = 'https://api.genusconnect.com/v1/carevoice/facility/' + this.carePartnerData.guid;
            this.awsServiceProvider.get(url).then(res => {
                console.log(res);
                let data: any = res;
                this.guid = data.guid
                if (this.guid === this.carePartnerData.guid) {
                    this.flag = true;
                    this.carePartnerData.guid = data.guid;
                    this.carePartnerData.partnerId = data.partnerId;
                    this.carePartnerData.parentId = data.parentId;
                    this.carePartnerData.name = data.name;
                    this.carePartnerData.nickname = data.nickname;
                    this.carePartnerData.address = data.address;
                    this.carePartnerData.city = data.city;
                    this.carePartnerData.state = data.state;
                    this.carePartnerData.postalCode = data.postalCode;
                    this.carePartnerData.partnerLevel = data.partnerLevel;
                    this.carePartnerData.status = data.status;
                    this.carePartnerData.phone = data.phone;
                    this.carePartnerData.lead = data.lead;
                    this.carePartnerData.notes = data.notes;
                    this.carePartnerData.department = data.department;
                    this.carePartnerData.reporting = data.reporting;
                    this.carePartnerData.location = data.location;
                    this.carePartnerData.longitude = data.longitude;
                    this.carePartnerData.latitude = data.latitude;
                    this.carePartnerData.place_id = data.place_id;
                }
            }).catch(err => {
                console.log(err);
                this.flag = false;
            })
        }
    }

    register(form: any) {
        debugger;
        // if (this.careFacilityForm.valid) {
        let careFacilityData = this.awsServiceProvider.getCareFacilityData();
        careFacilityData.guid = this.carePartnerData.guid,
            careFacilityData.partnerId = form.partnerId,
            careFacilityData.parentId = form.parentId,
            careFacilityData.name = form.name,
            careFacilityData.nickname = form.name,
            careFacilityData.address = form.address,
            careFacilityData.city = form.city,
            careFacilityData.state = form.state,
            careFacilityData.postalCode = form.postalCode,
            careFacilityData.partnerLevel = form.partnerLevel,
            careFacilityData.status = form.status,
            careFacilityData.phone = form.phone,
            careFacilityData.lead = form.lead,
            careFacilityData.notes = form.notes,
            careFacilityData.department = form.department,
            careFacilityData.reporting = form.reporting,
            careFacilityData.location = this.carePartnerData.location,
            careFacilityData.longitude = this.carePartnerData.lng,
            careFacilityData.latitude = this.carePartnerData.lat,
            careFacilityData.place_id = this.carePartnerData.place_id
        if (this.guid !== null && this.guid !== undefined && this.guid !== '') {
            let url = 'https://api.genusconnect.com/v1/carevoice/facility/' + careFacilityData.guid;
            this.awsServiceProvider.put(url, careFacilityData).then((res) => {
                console.log(res);
                this.carePartnerData = "";
                alert('Your care facility updated successfully!');
            })
                .catch((error) => {
                    console.log(error);
                })
        }
        else {
            let url = 'https://api.genusconnect.com/v1/carevoice/facility';
            this.awsServiceProvider.post(url, careFacilityData).then((res) => {
                console.log(res);
                this.carePartnerData = "";
                alert('Your care facility registered successfully!');
            })
                .catch((error) => {
                    console.log(error);
                })
        }

    }
    //}

}

import { Component } from '@angular/core';
import { NavController, NavParams, Keyboard, Loading, LoadingController, AlertController, Events } from 'ionic-angular';
import { CareFacilityPage } from '../care-facility/care-facility';
import { AuthProvider } from '../../providers/auth/auth';

@Component({
  selector: 'page-user-login',
  templateUrl: 'user-login.html',
})
export class UserLoginPage {
  
  loading: Loading;
  email: string = "";
  password: string = "";
  partOfEmail: string = "";
  emailLengthA: number;
  emailLengthB: number;
  emailLengthC: number;
  showPassword: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public keyboard: Keyboard,
    private loadingCtrl: LoadingController,
    public authProvider: AuthProvider,
    public alertCtrl: AlertController,
    public events: Events,) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserLoginPage');
  }

  // Login() {
  //   this.navCtrl.setRoot(CareFacilityPage);
  // }

  btnActiv() {
    var reg = new RegExp(/ /g, "");
    var withOutWhiteSpaces = this.email.replace(/ /g, "");
    var last = withOutWhiteSpaces.charAt(withOutWhiteSpaces.length - 1);
    var a = this.email.indexOf("@");
    var b = this.email.lastIndexOf(".");
    var c = this.email.lastIndexOf(last);
    var allEmail = this.email.slice(0, c);
    this.partOfEmail = this.email.slice(0, a);
    this.emailLengthA = this.partOfEmail.length;
    this.partOfEmail = this.email.slice(a, b);
    this.emailLengthB = this.partOfEmail.length;
    this.partOfEmail = this.email.slice(b, c);
    this.emailLengthC = this.partOfEmail.length;

    if ((this.email.indexOf('@') > -1) && (this.email.indexOf('.') > -1) && (this.emailLengthA >= 1) && (this.emailLengthB >= 2) &&
      (this.emailLengthC >= 2) && (this.emailLengthC <= 4) && (reg.test(allEmail) == false))
      document.getElementById('myBtn').removeAttribute("disabled");
    else document.getElementById('myBtn').setAttribute("disabled", "disabled");
  }

  onKeydown($event) {
    console.log($event);
    this.keyboard.close();
  }

  Login() {
    if (this.email === undefined || this.email === '' || this.email === null && this.password === undefined || this.password == null || this.password === "") {
      return
    }
    else {
      this.loading = this.loadingCtrl.create({
        content: 'Logging In...',
        dismissOnPageChange: true
      });
      this.loading.present();
      this.authProvider.authenticateWithEmail(this.email.toLowerCase(), this.password)
        .then((res) => {
          if (localStorage.getItem('CommunityData')) {
            this.navCtrl.setRoot(CareFacilityPage);
          }
          else {
            this.authProvider.getUserData()
              .then((res) => {
                console.log('Getting UserData');
                let userData: any = res;
                console.log(res);
                for (let i in userData.UserAttributes) {
                  if (userData.UserAttributes[i].Name == "sub") {
                    console.log(userData.UserAttributes[i].Value);
                    localStorage.setItem('CommunityData', userData.UserAttributes[i].Value);
                  }
                  if (userData.UserAttributes[i].Name == "given_name") {
                    console.log(userData.UserAttributes[i].Value);
                    localStorage.setItem('given_name', userData.UserAttributes[i].Value);
                  }
                  if (userData.UserAttributes[i].Name == "family_name") {
                    console.log(userData.UserAttributes[i].Value);
                    localStorage.setItem('family_name', userData.UserAttributes[i].Value);
                  }
                  let getUserName = localStorage.getItem('given_name') + " " + localStorage.getItem('family_name');
                  localStorage.setItem('getFullName', getUserName);
                  this.events.publish('user:AuthData', getUserName);
                };

                this.loading.dismissAll();
                this.navCtrl.setRoot(CareFacilityPage);

              });
          }

        })
        .catch((error) => {
          if (error.message === "Network error") {
            let alrtCtrl = this.alertCtrl.create({
              subTitle: error.message,
              message: 'Please check your network connection and try again....',
              buttons: [{
                text: "Ok",
                role: 'cancel'
              }]
            })
          }
          console.log(error.message);
          this.loading.dismissAll();
          let alrtCtrl = this.alertCtrl.create({
            subTitle: 'Invalid Authentication Details',
            message: 'Plese check email and password and try again...',
            buttons: [{
              text: "Ok",
              role: 'cancel'
            }]
          });
          alrtCtrl.present();
        })
    }

  }
}

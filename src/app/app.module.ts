import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { CareFacilityPage } from '../pages/care-facility/care-facility';
import { Geolocation } from '@ionic-native/geolocation';
import { AwsServiceProvider } from '../providers/aws-service/aws-service';
import { HttpClientModule } from '@angular/common/http'; 
import { CareFacilityListPage } from '../pages/care-facility-list/care-facility-list';
import { FooterComponent } from '../components/footer/footer';
import { HeaderComponent } from '../components/header/header';
import { UserLoginPage } from '../pages/user-login/user-login';
import { AuthProvider } from '../providers/auth/auth';
import { IonicStorageModule } from '@ionic/storage';
import { Device} from "@ionic-native/device";

@NgModule({
  declarations: [
    MyApp,
    CareFacilityPage,
    CareFacilityListPage,
    HeaderComponent,
    FooterComponent,
    UserLoginPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    CareFacilityPage,
    CareFacilityListPage,
    HeaderComponent,
    FooterComponent,
    UserLoginPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AwsServiceProvider,
    AuthProvider,
    Device,
  ]
})
export class AppModule {}

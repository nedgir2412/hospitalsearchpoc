import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UUID } from 'angular2-uuid';


@Injectable()
export class AwsServiceProvider {

  constructor(public http: HttpClient) {
    console.log('Hello AwsServiceProvider Provider');
  }

  getCareFacilityData() {
    return {
      "guid":"NA",
      "partnerId":"NA",
      "parentId":"NA",
      "name":"NA",
      "nickname":"NA",
      "address":"NA",
      "city":"NA",
      "state":"NA",
      "postalCode":"NA",
      "partnerLevel":"NA",
      "status":"NA",
      "phone":"NA",
      "lead":"NA",
      "notes":"NA",
      "department":"NA",
      "reporting":"NA",
      "createdDate": new Date(),
      "lastUpdateTimestamp": new Date(),
      "location":"NA",
      "longitude":"NA",
      "latitude": "NA",
      "place_id": "NA"
    }
  }

  post(url, body) {
    return new Promise((resolve, rejects) => {
      this.http.post(url, body).subscribe(
        res => {
          resolve(res)
        },
        error => {
          rejects(error)
        }
      );
    })
  }

  put(url, body) {
    debugger;
    return new Promise((resolve, rejects) => {
      this.http.put(url, body).subscribe(
        res => {
          resolve(res)
        },
        error => {
          rejects(error)
        }
      );
    })
  }

  get(url) {
    return new Promise((resolve, rejects) => {
      this.http.get(url).subscribe(
        res => {
          resolve(res)
        },
        error => {
          rejects(error)
        }
      );
    })
  }

}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CognitoUserPool, CognitoUser, AuthenticationDetails } from "amazon-cognito-identity-js";
import { Storage } from '@ionic/storage';
import { Device } from "@ionic-native/device";
import * as AWS from "aws-sdk/global";
import * as awsservice from "aws-sdk/lib/service";
import { ENV } from '../../config/environment';
import { CognitoIdentity } from 'aws-sdk';

@Injectable()
export class AuthProvider {

  public static IDENTITY_POOL_ID = ENV.IDENTITY_POOL_ID;
  public static USER_POOL_ID = ENV.USER_POOL_ID;
  public static CLIENT_ID = ENV.CLIENT_ID;
  public static REGION = ENV.REGION;

  constructor(
    public http: HttpClient,
    public device: Device,
    public storage: Storage,
    ) {
    console.log('Hello AuthProvider Provider');
  }

  public static POOL_DATA: any = {
    UserPoolId: AuthProvider.USER_POOL_ID,
    ClientId: AuthProvider.CLIENT_ID
  };

  public cognitoCreds: AWS.CognitoIdentityCredentials;

  getUserPool() {
    return new CognitoUserPool(AuthProvider.POOL_DATA);
  }

  buildCognitoCreds(idTokenJwt: string) {
    let url = 'cognito-idp.' + AuthProvider.REGION.toLowerCase() + '.amazonaws.com/' + AuthProvider.USER_POOL_ID;
    let logins: CognitoIdentity.LoginsMap = {};
    logins[url] = idTokenJwt;
    let params = {
      IdentityPoolId: AuthProvider.IDENTITY_POOL_ID,
      Logins: logins
    };
    let serviceConfigs: awsservice.ServiceConfigurationOptions = {};
    let creds = new AWS.CognitoIdentityCredentials(params, serviceConfigs);
    this.setCognitoCreds(creds);
    return creds;
  }

  setCognitoCreds(creds: AWS.CognitoIdentityCredentials) {
    this.cognitoCreds = creds;
}

  getCurrentUser() {
    let cognitoUser: CognitoUser = this.getUserPool().getCurrentUser();
    if (cognitoUser != null) {
      cognitoUser.getSession(function (err, session) {
        if (err) {
          //console.log(err);
          return;
        }
      });
    }
    // console.log(cognitoUser);
    return cognitoUser;
  }

  //   async isAuthenticated() {
  //     let loggedIn = await this.storage.get('is_loggedin');
  //     let url = 'cognito-idp.' + this.REGION.toLowerCase() + '.amazonaws.com/' + this.USER_POOL_ID;
  //     if (!loggedIn) {
  //       return false;
  //     } else {
  //       let loginToken = await this.storage.get('login_token');
  //       AWS.config.update({
  //         region: this.REGION.toLowerCase(),
  //         credentials: new AWS.CognitoIdentityCredentials({
  //           IdentityPoolId: this.IDENTITY_POOL_ID
  //         })
  //       });
  //     }
  //     return true;
  //   }

  //   async isRegistered() {
  //     let status = await this.storage.get('is_registered');
  //     if (!status) {
  //       return false;
  //     }
  //     return true;
  //   }

  //   authenticate() {
  //     let username = this.device.uuid + '@carevoice.com';
  //     let password = 'Password!' + this.device.uuid;
  //     let authenticationData = {
  //       Username: username,
  //       Password: password,
  //     };
  //     let authenticationDetails = new AuthenticationDetails(authenticationData);
  //     let userData = {
  //       Username: username,
  //       Pool: this.cognitoUtil.getUserPool()
  //     };
  //     let cognitoUser = new CognitoUser(userData);
  //     let self = this;

  //     return new Promise((resolve, reject) => {
  //       cognitoUser.authenticateUser(authenticationDetails, {
  //         newPasswordRequired: function (userAttributes, requiredAttributes) {
  //           console.log('User needs to set password.');
  //           reject(false);
  //         },
  //         onSuccess: result => {
  //           console.log(result);
  //           let creds = self.cognitoUtil.buildCognitoCreds(result.getIdToken().getJwtToken());
  //           AWS.config.credentials = creds;
  //           let clientParams: any = {};
  //           let sts = new STS(clientParams);
  //           let Data;
  //           sts.getCallerIdentity((err, data) => {
  //             let apigClient = apigClientFactory.newClient();
  //             apigClient.communityLoadorcreateuserPost([], {
  //               username: username,
  //               cognitoId: this.cognitoUtil.getCognitoIdentity()
  //             }).then(res => {
  //               data = res;
  //               console.log('User details');
  //             });
  //           });
  //           this.storage.set('is_loggedin', true);
  //           this.storage.set('login_token', result.getIdToken().getJwtToken());
  //           this.awsCredits = this.cognitoUtil.buildCognitoCreds(localStorage.getItem('login_token'));
  //           AWS.config.credentials = this.awsCredits;
  //         },
  //         onFailure: function (err) {
  //           console.log(err.message);
  //           reject(false);
  //         },
  //       });
  //     });
  //   }

  authenticateWithEmail(email, pwd) {
    let username = email;
    let password = pwd;
    let authenticationData = {
      Username: username,
      Password: password,
    };
    let authenticationDetails = new AuthenticationDetails(authenticationData);
    let userData = {
      Username: username,
      Pool: this.getUserPool()
    };
    let cognitoUser = new CognitoUser(userData);
    let self = this;
    return new Promise((resolve, reject) => {
      cognitoUser.authenticateUser(authenticationDetails, {
        newPasswordRequired: function (userAttributes, requiredAttributes) {
          console.log('User needs to set password.');
          reject(false);
        },
        onSuccess: result => {
          console.log('cognito-authentication');
          let creds = self.buildCognitoCreds(result.getIdToken().getJwtToken());
          AWS.config.credentials = creds;
          console.log(creds);
          this.storage.set('is_loggedin', true);
          this.storage.set('login_token', result.getIdToken().getJwtToken());
          resolve(true);

        },
        onFailure: function (err) {
          console.log(err.message);
          reject(false);
        },
      });
    });
  }

  //   register_one(email, pwd, fname, lastname) {
  //     let cognitoParams = {
  //       UserPoolId: ENV.USER_POOL_ID,
  //       ClientId: ENV.CLIENT_ID,
  //       IdentityPoolId: ENV.IDENTITY_POOL_ID,
  //     };

  //     //getUserPool
  //     let userPool = new CognitoUserPool({
  //       UserPoolId: cognitoParams.UserPoolId,
  //       ClientId: cognitoParams.ClientId // Your client id here
  //     });

  //     let attributes = [];
  //     attributes.push(new CognitoUserAttribute({
  //       Name: 'email',
  //       Value: email
  //     }));

  //     attributes.push(new CognitoUserAttribute({
  //       Name: 'given_name',
  //       Value: fname
  //     }));

  //     attributes.push(new CognitoUserAttribute({
  //       Name: 'family_name',
  //       Value: lastname
  //     }));

  //     return new Promise((resolve, reject) => {
  //       userPool.signUp(email, pwd, attributes, null, (err, data) => {
  //         if (err) {
  //           console.log(err);
  //           if (err['code'] == "UsernameExistsException") {
  //             this.storage.set('is_registered', true);
  //           }
  //           reject(err);
  //         } else {
  //           console.log(data);
  //           this.storage.set('is_registered', true);
  //           resolve(true);
  //         }
  //       });
  //     });
  //   }

  //   // Get Current User Data
  getUserData() {
    return new Promise((resolve, rejects) => {
      this.getCurrentUser()
        .getUserData((error, data) => {
          resolve(data);
        })
    })
  }

  //   logOutUser() {
  //     return new Promise((resolve, rejects) => {
  //       this.cognitoUtil.getCurrentUser().signOut();
  //       // this.cognitoUtil.cognitoCreds.clearCachedId();
  //       this.storage.remove('is_loggedin');
  //       localStorage.removeItem('CommunityData');
  //       localStorage.removeItem('getFullName');
  //       localStorage.removeItem('login_token');
  //       resolve(true);
  //     })
  //   }

  //   getCongoId(idTokenJwt: string) {
  //     let url = 'cognito-idp.' + this.REGION.toLowerCase() + '.amazonaws.com/' + this.USER_POOL_ID;
  //     let logins: Array<string> = [];
  //     logins[url] = idTokenJwt;
  //     let params = {
  //       IdentityPoolId: this.IDENTITY_POOL_ID, /* required */
  //       Logins: logins
  //     };

  //     console.log(params);
  //     return params;
  //   }

  //   userLogOut() {
  //     console.log('Data removed');
  //     this.storage.remove('is_loggedin');
  //   }

}
